angular.module("bchile-directives", ['']);
angular.module('bchile-directives.carta-instruccion',['ui.bootstrap']).directive('representativeSelector', function () {
    return {
        restrict: 'E',
        scope: {
            source: '=',
            target: '=ngModel',
            isArray: '@'

        },
        require: '?ngModel',
        templateUrl: 'directives/carta-instruccion/representative-selector.tpl.html',
        link: function(scope,elements,attr) {
            console.info("elements",elements);
            console.info('atrr',attr);
            console.info("scope",scope);
            scope.addRepresentative = function(representative) {
                if(attr.isArray) {
                    addToArray(representative);
                }else{
                    scope.target = representative;
                }
            };
            scope.removeRepresentative = function(representative) {
                if (attr.isArray) {
                    removeFromArray(representative);
                }else {
                    scope.target = null;
                }
            };
            var removeFromArray = function(representative) {
                _.pull(scope.target,representative);
                scope.source.push(representative);
            };

            var addToArray = function(representative) {
                _.pull(scope.source,representative);
                scope.target.push(representative);
            };

        }
    };
});